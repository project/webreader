<?php

/**
 * @file
 * Include file for data definitions.
 */

/**
 * Returns webReader supported languages.
 * @return array
 *   Language Array
 */
function webreader_language_list() {
  $webreader_languages = array(
    'ca' => array(
      'langid' => 'ca_es',
      'alttext' => 'Escolteu aquesta plana utilitzant webReader',
    ),
    'da' => array(
      'langid' => 'da_dk',
      'alttext' => 'Lyt til denne side med webReader',
    ),
    'de' => array(
      'langid' => 'de_de',
      'alttext' => 'Vorlesen mit webReader',
    ),
    'en' => array(
      'langid' => 'en_us',
      'alttext' => 'Listen with webReader',
    ),
    'es' => array(
      'langid' => 'es_es',
      'alttext' => 'Leer con webReader',
    ),
    'eu' => array(
      'langid' => 'es_eu',
      'alttext' => 'Orri hau entzun webReader erabiliz',
    ),
    'fi' => array(
      'langid' => 'fi_fi',
      'alttext' => 'Kuuntele webReaderilla',
    ),
    'fr' => array(
      'langid' => 'fr_fr',
      'alttext' => 'Lis moi avec webReader',
    ),
    'gl' => array(
      'langid' => 'gl_es',
      'alttext' => 'Ler con webReader',
    ),    
    'it' => array(
      'langid' => 'it_it',
      'alttext' => 'Leggi con webReader',
    ),
    'jp' => array(
      'langid' => 'ja_jp',
      'alttext' => 'Listen to this page using webReader',
    ),
    'ko' => array(
      'langid' => 'ko_kr',
      'alttext' => 'Listen to this page using webReader',
    ),
    'nl' => array(
      'langid' => 'nl_nl',
      'alttext' => 'Laat de tekst voorlezen met webReader',
    ),
    'nb' => array(
      'langid' => 'no_nb',
      'alttext' => 'Lytt til denne siden med webReader',
    ),
    'pt-pt' => array(
      'langid' => 'pt_pt',
      'alttext' => 'Ouvir com webReader',
    ),
    'pt' => array(
      'langid' => 'pt_pt',
      'alttext' => 'Ouvir com webReader',
    ),
    'sv' => array(
      'langid' => 'sv_se',
      'alttext' => 'L&auml;s med webReader',
    ),
  );

  $webreader_prefer_uk = variable_get('webreader_prefer_uk', FALSE);
  if ($webreader_prefer_uk) {
    $webreader_languages['en']['langid'] = 'en_uk';
  }

  return $webreader_languages;
}
