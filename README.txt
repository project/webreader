; Module name
webReader module

; Drupal Version
Drupal 7.x

; Description
This module integrates the webReader to your Drupal 7 website

; Prerequisites
An own webReader account and locale.module may be useful

; Installation
To install, copy the webreader directory and all its contents to your modules
directory.

; Update from earlier webReader module versions
Please check settings under administer -> configuration-> content -> webReader and
submit settings to init possible new options.

; Configuration
To enable this module, visit administer -> modules, and enable webReader module.
Configure settings under administer -> configuration-> content -> webReader.

; Bugs/Features/Patches:
If you want to report bugs, feature requests, or submit a patch, please do so
at the project page on the Drupal web site.
http://drupal.org/project/webreader

; Author
keopx (http://www.keopx.net)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.

The author can also be contacted for paid customizations of this
and other modules.
