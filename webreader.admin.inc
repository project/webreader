<?php

/**
 * @file
 * Administrative page callbacks for the webreader module. 
 */

/**
 * Definition for the WebReader configuration form.
 *
 * General settings form for webReader
 * Layout settings form for webReader
 * Node type selection form for webReader
 */
function webreader_admin_settings($form, &$form_state) {

  module_load_include('inc', 'webreader', 'webreader.pages');
  $webreader_languages = webreader_language_list();
  $languages = language_list();

  $lang_list = array();
  foreach ($languages as $language) {
    if (array_key_exists($language->language, $webreader_languages)) {
      $lang_list[$language->language] = t($language->name);
    }
  }
  // Required settings for webReader.
  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('General settings for webReader'),
    '#description' => t('The webReader module requires to create an own account at <a href="http://webreader.readspeaker.com" target="_blank">http://webreader.readspeaker.com</a>. For private websites you can get webReader for free.'),
  );
  $form['general_settings']['webreader_accountid'] = array(
    '#type' => 'textfield',
    '#title' => t('Enter your webReader ID'),
    '#default_value' => variable_get('webreader_accountid', ''),
    '#description' => t('Enter your webReader ID from <a href="http://webreader.readspeaker.com" target="_blank">http://webreader.readspeaker.com</a>.'),
    '#required' => TRUE,
  );
  $form['general_settings']['webreader_langid'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#default_value' => variable_get('webreader_langid', ''),
    '#options' => $lang_list + array('all' => t('All languages')),
    '#description' => t('Select which language your webReader account supports (free webReader supports only one language).'),
    '#required' => TRUE,
  );
  $form['general_settings']['webreader_prefer_uk'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prefer british english'),
    '#default_value' => variable_get('webreader_prefer_uk', FALSE),
    '#description' => t('Check this option, if you prefer british english.'),
    '#required' => FALSE,
  );

  // Layout settings for input webReader into node.
  $form['layout_settings'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Layout settings'),
    '#description' => t('Options to control display position of webReader on your website.'),
  );
  $form['layout_settings']['webreader_abovenode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display webReader control above node text.'),
    '#default_value' => variable_get('webreader_abovenode', FALSE),
    '#description' => t('Insert webReader controls between node title and node text.'),
    '#required' => FALSE,
  );
  $form['layout_settings']['webreader_belownode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display webReader control below node text.'),
    '#default_value' => variable_get('webreader_belownode', FALSE),
    '#description' => t('Insert webReader controls after node text.'),
    '#required' => FALSE,
  );

  // What kinds of nodes do we want to read by webReader.
  $form['content_types'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#title' => t('Content types'),
  );
  $form['content_types']['webreader_nodes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#default_value' => variable_get('webreader_nodes', array()),
    '#options' => array_map('check_plain', node_type_get_names()),
    '#description' => t('Select content types to read by webReader.'),
  );

  return system_settings_form($form);
}
